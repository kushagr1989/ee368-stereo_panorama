%x=[0 20 45 90 135 150 180]*pi/180;
%y=[198 210 264 315 192 141 93]*2.54/100;
function [v]=Interpolation(Num_Im)
x=linspace(0,pi,Num_Im);
%x=0:3000;
%Linear model Poly5:
 
%Coefficients (with 95% confidence bounds):
p1 =     0.03005  ;
p2 =      0.6894  ;
p3 =      -5.003  ;
p4 =       8.196  ;
p5 =      -1.522  ;
p6 =       5.038 ;
       
z = p1.*(x).^5 + p2.*(x).^4 + p3.*(x).^3 + p4.*(x).^2 + p5.*(x)/ + p6;      
plot(Num_Im/pi*x,z);

%%
R=47/100;
alpha=0.5*pi/180;
d=sin(alpha)*z;
f=4/1000;

im_width=1920;
SCALE=1/4*10^4*im_width/2*0.8;
beta=asin(d/R);
v=tan(beta)*f*SCALE+100;
plot(v)
