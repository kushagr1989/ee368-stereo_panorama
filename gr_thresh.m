function [Im_out] = gr_thresh(Im_in,gr_th)

[m,n]=size(Im_in);
for i=1:m
    for j=1:n
        if(Im_in(i,j)<(gr_th*255))
            Im_out(i,j)=0;
        else
            Im_out(i,j)=255;
        end
    end
end