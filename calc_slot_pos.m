% function to calculate the position of strip for left and right stereo panorama images

d=0.07; % diameter for baseline =6.5 cm
R=0.15; % arm length(center of rotation to optical centre ) (10-15 cm)
beta=asin(d/R);
f=1864.5; % focal length of camera (in pixels)
v=tan(beta)*f % distance from the centre


