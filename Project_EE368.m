clc
clear
% Data_Parameters
dir_name='frames\MOV_2727_30\';
number=30;
src_db=dir(dir_name);
Num_Im=length(src_db)-2
%%
strip_width=15;
[row,col,ch]=size(imread(strcat(dir_name,src_db(10).name)));

left_strip_pos=col/2-40;
right_strip_pos=col/2+40;
Im_width=strip_width*Num_Im;
Im_cmpnd_left_eye=zeros(row,Im_width,3);
Im_cmpnd_right_eye=zeros(row,Im_width,3);
tic
idx=1;
for id = 3:length(src_db)
    filename = strcat(dir_name,src_db(id).name);
    Im_DB= imread(filename); 
    start=(idx-1)*strip_width + 1;
    last=idx*strip_width;
    Im_cmpnd_left_eye(:,start:last,:)=double(Im_DB(:,right_strip_pos+1:right_strip_pos+strip_width,:)); % form the compound Image for left eye
    Im_cmpnd_right_eye(:,start:last,:)=double(Im_DB(:,left_strip_pos+1:left_strip_pos+strip_width,:)); % form the compound Image for right eye
    idx=idx+1;
end
toc
figure,imshow(uint8(Im_cmpnd_left_eye))    
figure, imshow(uint8(Im_cmpnd_right_eye))
imwrite(uint8(Im_cmpnd_left_eye),strcat('panorama\',num2str(number),'_left_eye_Im.jpg'));
imwrite(uint8(Im_cmpnd_right_eye),strcat('panorama\',num2str(number),'_right_eye_Im.jpg'));

%% ALIGNMENT OF DATA FOR NO DISPARTY AT INFINITY
num=30;
Im_1=imread(strcat('panorama\',num2str(number),'_left_eye_Im.jpg'));
Im_2=imread(strcat('panorama\',num2str(number),'_right_eye_Im.jpg'));
Im_1gr=double(rgb2gray(Im_1));
Im_2gr=double(rgb2gray(Im_2));
%%
% Image portion to consider for no disparity
pos1=2835;
pos2=2900;
del=pos2-pos1;
Im_1_cut=Im_1gr(:,pos1:pos2);

for id=1:size(Im_2gr,2)-del
    Im_2_temp=Im_2gr(:,id:id+del)-Im_1_cut;
    vr(id)=var(Im_2_temp(:));
end

[val,x_cor]=sort(vr);
shift_x=x_cor(1)-pos1; % for right image
Im_2_align=zeros(size(Im_2));
Im_2_align(:,shift_x+1:end,:)= double(Im_2(:,1:end-shift_x,:));
figure,plot(vr)
% [ht,wd]=size(Im_1gr);
% CorM=normxcorr2(Im_2gr,Im_1gr);
% [max_val,ind]=max(abs(CorM(:)));
% [yp,xp]=ind2sub(size(CorM),ind);
% dy=yp-ht;dx=xp-wd;
% A = [1 0 dx; 0 1 dy; 0 0 1];
% tform = maketform('affine', A.');
% [height, width,ch] = size(Im_2);
% Im_2_align = imtransform(Im_2, tform, 'bilinear', ...
%     'XData', [1 width], 'YData', [1 height], ...
%     'FillValues', zeros(ch,1));

%figure, imshow(Im_1)
%figure, imshow(Im_2_align)
figure, imshow(Im_1-uint8(Im_2_align))

%% DISPARITY COMPUTATION
[r1,c1,ch]=size(Im_2_align);
win=50;
r=[100:300:900];   
cm=floor(c1/win); %for each column
pos=0;
Im_1_dbl=rgb2gray(double(Im_1)./double(max(Im_1(:))));
Im_2_dbl=rgb2gray(double(Im_2_align)./double(max(Im_2_align(:))));

for pos=1:length(r)
    idx=1;
    for c=1:c1
        if((c-win/2)>=1 && (c+win/2)<=c1)
            Im_temp=Im_1_dbl(r(pos)-win/2:r(pos)+win/2,c-win/2:c+win/2)-Im_2_dbl(r(pos)-win/2:r(pos)+win/2,c-win/2:c+win/2);
            vr=var(Im_temp(:));
            std(pos,idx)=sqrt(vr);
            idx=idx+1;
        end
    end
end

for id=1:length(r)  %% plots for disparity
subplot(length(r),1,id)
plot(std(id,:))
end

[m,n]=size(std);
final_max=zeros(n,1);
% take the maximum
for id=1:n
    temp_val=max(std(:,id));
    final_max(id)=temp_val;
end
figure,plot(final_max)
%%
clear filt_out

% for id=1:7
% filt_out{id}=medfilt1(final_max,id*10);
% end
% 
% for id=1:length(filt_out)  %% plots for filtered output
% subplot(length(filt_out),1,id)
% plot(filt_out{id})
% end
% chosen filter size =20
filt_out=medfilt1(final_max,400);
figure
subplot(2,1,1)
plot(final_max)
subplot(2,1,2)
plot(filt_out)

%% IMPLEMENTING ADC
max_var=max(filt_out);
min_var=min(filt_out);
thresh=0.2;
strip_gap=zeros(c1,1);

for id=1:length(filt_out)
    if(filt_out(id)<= thresh)
        strip_gap(id)=round(80+140*(thresh-filt_out(id))/(thresh-min_var));
    elseif(filt_out(id) > thresh)
        strip_gap(id)=round(80+30*(thresh-filt_out(id))/(max_var-thresh));
    end
end
strip_gap(length(filt_out)+1:c1)=strip_gap(length(filt_out));
figure,plot(strip_gap)        
%%
strip_width=15;
Im_width=strip_width*(Num_Im);
[row,col,ch]=size(imread(strcat(dir_name,src_db(10).name)));
Im_centre=col/2;

right_strip_pos=round(Im_centre+strip_gap./2); % for left eye images (as calculated for reference as left)

left_strip_pos=zeros(1,length(strip_gap));
left_strip_pos(1:shift_x)=round(Im_centre-strip_gap(end-shift_x+1:end)./2);
left_strip_pos(shift_x+1:end)=round(Im_centre-strip_gap(1:end-shift_x)./2);

figure,plot(left_strip_pos)
figure, plot(right_strip_pos)

%%

Im_cmpnd_left_eye_1=zeros(row,Im_width,3);
Im_cmpnd_right_eye_1=zeros(row,Im_width,3);
tic
idx=1;
for id = 3:length(src_db)
    filename = strcat(dir_name,src_db(id).name);
    Im_DB= imread(filename); 
    start=(idx-1)*strip_width + 1;
    last=idx*strip_width;
    Im_cmpnd_left_eye_1(:,start:last,:)=double(Im_DB(:,right_strip_pos(idx)+1:right_strip_pos(idx)+strip_width,:)); % form the compound Image for left eye
    Im_cmpnd_right_eye_1(:,start:last,:)=double(Im_DB(:,left_strip_pos(idx)+1:left_strip_pos(idx)+strip_width,:)); % form the compound Image for right eye
    idx=idx+1;
end
toc
%% show data
figure,imshow(uint8(Im_cmpnd_left_eye_1))    
figure, imshow(uint8(Im_cmpnd_right_eye_1))

imwrite(uint8(Im_cmpnd_left_eye_1),strcat('panorama\',num2str(number),'_left_eye_Im_with_depth.jpg'));
imwrite(uint8(Im_cmpnd_right_eye_1),strcat('panorama\',num2str(number),'_right_eye_Im_with_depth.jpg'));








